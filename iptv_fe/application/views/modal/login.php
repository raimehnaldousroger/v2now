<<<<<<< HEAD
<template>
	<div id="loginDiv" class="loginDiv loginDivShow">
		<div class="form-area">
			<div class="wrapper">
				<div class="alert d-n">
					<h3 class="alert-title"></h3>
				</div>
				<h1 class="form-title">Login</h1>
				<!-- login-->
				<form
					method="POST"
					id="loginForm"
					class="loginForm form-wrapper"
				>
					<div class="input-wrapper">
						<label for="">Username</label>
						<input
							type="email"
							name="email"
							id="email"
							autocomplete="off"
							required
							maxlength="150"
						/>
					</div>
					<div class="input-wrapper">
						<label for="">Password</label>
						<input
							type="password"
							name="password"
							autocomplete="off"
							id="password"
							required
							maxlength="50"
						/>
					</div>
					<div class="input-wrapper">
						<button class="login-button" @click="login" type="submit">
							Login
						</button>
					</div>
					<div class="input-wrapper">
						<div class="text-center">
							<span>Not yet a member?</span>
							<a href="javascript:void(0)" id="register-link"
								>Register now</a
							>
						</div>
					</div>
				</form>

				<!-- register-->
				<form
					method="POST"
					class="register form-wrapper d-n"
					id="register-frm"
				>
					<div class="input-wrapper">
						<label for="">First Name</label>
						<input
							type="text"
							name="firstname"
							maxlength="30"
						/>
					</div>
					<div class="input-wrapper">
						<label for="">Last Name</label>
						<input type="text" name="lastname" maxlength="30" />
					</div>
					<div class="input-wrapper">
						<label for="">Email</label>
						<input type="email" name="email" maxlength="50" />
					</div>
					<div class="input-wrapper">
						<label for="">Password</label>
						<input
							type="password"
							name="password"
							maxlength="20"
						/>
					</div>
					<div class="input-wrapper term">
						<input type="checkbox" name="terms" id="terms" />
						<label class="text-left"
							>I understand and agree to the
							<a
								href="#"
								class="terms-link"
								id="termsAndCondition"
								>Terms and Conditions of Use</a
							>.</label
						>
=======

<div id="loginDiv" class="loginDiv loginDivShow">
	<div class="form-area" id="loginRegisterApp">
		<div class="wrapper">
			<h1 class="form-title" v-if="loginModalStatus =='1'">Login</h1>
			<h1 class="form-title" v-if="registerModalStatus =='1'">Register</h1>
			<!-- login-->
			<form class="loginForm form-wrapper" v-if="loginModalStatus == true">
				<div class="input-wrapper">
					<label for="">Email</label>
					<input name="email" type="email" id="email" v-model="login.email">
				</div>
				<div class="input-wrapper">
					<label for="">Password</label>
					<input type="password" name="password" v-model="login.password">
				</div>
				<div class="input-wrapper">
					<button class="login-button" type="button" @click="loginBtn"> Login </button>
				</div>
				<div class="input-wrapper">
					<div class="text-center">
						<span>Not yet a member?</span>
						<a id="register-link" @click="changeModalStatus()">Register now</a
						>
					</div>
				</div>
			</form>

			<!-- register-->
			<form class="register form-wrapper" id="register-frm" v-if="registerModalStatus == true">
				<div class="input-wrapper">
					<label for="">First Name</label>
					<input type="text" name="firstname" v-model="register.first_name">
				</div>
				<div class="input-wrapper">
					<label for="">Last Name</label>
					<input type="text" name="lastname" v-model="register.last_name">
				</div>
				<div class="input-wrapper">
					<label for="">Email</label>
					<input type="email" name="email" v-model="register.email">
				</div>
				<div class="input-wrapper">
					<label for="">Password</label>
					<input type="password" name="password" v-model="register.password">
				</div>
				<div class="input-wrapper term">
					<input type="checkbox" name="terms" id="terms" />
					<label class="text-left">I understand and agree to the
						<a href="#" class="terms-link" id="termsAndCondition">Terms and Conditions of Use</a>.</label
					>
				</div>
				<div class="input-wrapper">
					<input class="register-btn" type="button" name="submit" value="Register" @click="registerBtn()">
				</div>

				<div class="input-wrapper">
					<div class="text-center">
						<span>Already a member?</span>
						<a id="login-link" @click="changeModalStatus()">Login here.</a>
>>>>>>> e5c691223569484a29e545edc7767621613edbda
					</div>
					<div class="input-wrapper">
						<input
							class="register-btn"
							type="submit"
							name="submit"
							value="Register"
						/>
					</div>

					<div class="input-wrapper">
						<div class="text-center">
							<span>Already a member?</span>
							<a href="javascript:void(0)" id="login-link"
								>Login here.</a
							>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>						
</template>