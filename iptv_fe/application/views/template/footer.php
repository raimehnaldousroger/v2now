

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js" integrity="sha512-jNDtFf7qgU0eH/+Z42FG4fw3w7DM/9zbgNPe3wfJlCylVDTT3IgKW5r92Vy9IHa6U50vyMz5gRByIu4YIXFtaQ==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/libraries/duDialog.min.js') ?>"></script>
<script src="<?= base_url('/assets/js/libraries/fontawesome-free-5.14.0-web/js/all.js') ?>"></script>

<script src="<?= base_url('assets/js/app.js')?>"></script>
<script src="<?= base_url('/assets/js/config.js') ?>"></script>
<script src="<?= base_url('assets/js/modules/loginRegister.js')?>"></script>
<script src="<?= base_url('assets/js/modules/livestream.js')?>"></script>
<script src="<?= base_url('assets/js/modules/trending.js')?>"></script>
<script src="<?= base_url('assets/js/modules/watch.js')?>"></script>
<script src="<?= base_url('assets/js/modules/channelWidget.js')?>"></script>
<script src="<?= base_url('assets/js/modules/explore.js')?>"></script>
<script src="<?= base_url('assets/js/modules/read.js')?>"></script>
<script src="<?= base_url('assets/js/modules/local.js')?>"></script>
<script src="<?= base_url('assets/js/modules/share.js')?>"></script>
<script>
    var tag = document.createElement("script");
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
</script>

</body>
	</body>
</html>
