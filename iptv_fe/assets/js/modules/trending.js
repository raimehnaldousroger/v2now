const trending = new Vue({
	el: "#trending-section",
	data: {
		now: [],
		hot: [],
		wow: [],
		videoId: "",
	},
	mounted() {
		this.setList();
		$("#trending").show();
	},
	methods: {
		setList: async function () {
			let result = await axios.get(endPoints.trending + "getTrending");
			this.now = result.data.now;
			this.hot = result.data.hot;
			this.wow = result.data.wow;
		},
		playSelect: function (id) {
			this.videoId = id;
			player.destroy();
			this.initYoutube();
			mainPlayerDiv
				.removeClass("hideMainPlayerDiv")
				.addClass("showMainPlayerDiv");
		},
		initYoutube() {
			player = new YT.Player("mainPlayer", {
				height: "100%",
				width: "100%",
				videoId: this.videoId,
				playerVars: { autoplay: 1, controls: 0 },
				events: {
					onReady: this.onPlayerReady,
					onStateChange: this.onPlayerStateChange,
				},
			});
		},
		onPlayerReady(evt) {
			evt.target.playVideo();
			evt.target.mute();
		},
		onPlayerStateChange(evt) {},
		saveFavorited: function (id, type = "program") {
			$.post(
				endPoints.favorite + "save",
				{ id: id, type: type },
				function (data) {
					if (data["success"] === true) {
						new duDialog("", data["msg"]);
					} else {
						duDialog(null, data["msg"], {
							buttons: duDialog.OK_CANCEL,
							okText: "Login",
							callbacks: {
								okClick: function (e) {
									this.hide();
									logout();
								},
							},
						});
					}
				},
				"json"
			);
		},
		share: function (id, img) {
			let url = split_url[0] + "?id=" + id;
			$(".share-img img").attr("src", img);
			$(".share-url").val(url);
			$("#share-modal").show();
		},
	},
});

$("#mainPlayerDiv .backButton").on("click", "button", function (e) {
	$(".fa-right").show();
	player.destroy();
	mainPlayerDiv.removeClass("showMainPlayerDiv").addClass("hideMainPlayerDiv");
	watch.initYoutube();
});
