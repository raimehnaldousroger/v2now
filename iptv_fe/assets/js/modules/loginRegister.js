const loginApp = new Vue({
	el: "#loginRegisterApp",
	data: {
		login: {
			email: '',
			password: '',
		},
		loginModalStatus: true,
		registerModalStatus: false,

		register: {
			first_name: '', 
			last_name: '',
			email: '',
			password: '',
		},

		hasSession: [],
	},
	mounted() {
		this.isLoggin();
	},
	methods: {
		isLoggin: async function() {
			await axios.get(endPoints.member+'sessionChecker')
              	 	   .then( (response) => {
              	 	   		this.hasSession = response.data.msg.member_logged_in_status;
              	 	   		if ( this.hasSession === true ) {
                 				$('#loginDiv').hide();
              	 	   		}
              		   })
              		   .catch( (error) => {
                		    // toastr.error('Session Timeout');
              		   })
		},
		// hide/show of modals;
		changeModalStatus: async function () {
			if (this.loginModalStatus == true) {
				this.loginModalStatus = false;
				this.registerModalStatus = true;
			} else {
				this.loginModalStatus = true;
				this.registerModalStatus = false;
			}
		},


		loginBtn: async function (e) {	
    		e.preventDefault();
            var bodyFormData = new FormData();
            bodyFormData.set('email', this.login.email);
            bodyFormData.set('password', this.login.password);
            
            await axios.post(endPoints.member+'login', bodyFormData)
              	 .then( (response) => {
                 	toastr.success('Yeah!');
                 	$('#loginDiv').hide();
              	})
              	.catch( (error) => {
                	toastr.error(error.response.data.msg);
              	})
		},

		registerBtn: async function () {
            var bodyFormData = new FormData();
            bodyFormData.set('email', this.register.email);
            bodyFormData.set('password', this.register.password);
            bodyFormData.set('first_name', this.register.first_name);
            bodyFormData.set('last_name', this.register.last_name);

            await axios.post(endPoints.member+'register', bodyFormData)
              	 .then( (response) => {
                 	toastr.success('Yeah!');
                 	this.loginModalStatus = true;
					this.registerModalStatus = false;
              	})
              	.catch( (error) => {
                	toastr.error(error.response.data.msg);
              	})
		}
	},
});
