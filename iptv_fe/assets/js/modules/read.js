const read = new Vue({
	el: "#read-section",
	data: {
		categories: [],
		articles: [],
		isActive: true,
		indexId: 0,
		isHidden: false,
	},
	mounted() {
		this.setCategories();
	},
	methods: {
		setCategories: async function () {
			let result = await axios.post(endPoints.read + "getCategories/", {
				id: id,
			});
			this.categories = result.data;
			this.getArticles(1);
		},
		getArticles: async function (id) {
			let result = await axios.post(endPoints.read + "getArticle/", {
				id: id,
			});
			this.articles = result.data;
		},
		showArticle: async function (id, index) {
			this.indexId = index;
			this.getArticles(id);
		},
		readDetails: function (banner, title, blurb, thumbnail) {
			let data = {
				banner: banner,
				title: title,
				blurb: blurb,
				thumbnail: thumbnail,
			};
			details.showDetails(data);
		},
		showHide: function (bool) {
			this.isHidden = bool;
		},
	},
});

const details = new Vue({
	el: "#read-details-section",
	data: {
		banner: "",
		title: "",
		blurb: "",
		thumbnail: "",
	},
	methods: {
		showDetails: function (data) {
			this.banner = data.banner;
			this.title = data.title;
			this.blurb = data.blurb;
			this.thumbnail = data.thumbnail;

			$("#trending").hide();
			$("#watch").hide();
			$("#live-section").hide();
			$("#explore").hide();
			$(".link-read").click();
			$("#read").hide();

			$("#read-promp-section").show();
		},
		close: function () {
			$("#read").show();
			$("#read-promp-section").hide();
			$("#trending").show();
			$("#watch").show();
			$("#live-section").show();
			$("#explore").show();

			$(".link-read").click();
		},
	},
});
