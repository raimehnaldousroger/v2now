const livestream_api = "/v1/iptv_api/livestream/show";
const video_wrapper = $(".video-wrapper");
const livestream_iframe = video_wrapper.find(".livestream-iframe");

$(function () {
	$(".details").hide();
	$.getJSON(livestream_api, function ({ result }) {
		if (result.length > 0) {
			$(".details").show();
			if (parseInt(result[0]["show"]) === 1) {
				$(".video-wrapper").hide();
				$(".notice").text(result[0]["title"]);
				$(".headline").html(`(${result[0]["blurb"]},
                        <span class="source">From ${result[0]["name"]}</span>)`);
			} else {
				$(".content-wrapper").hide();
				datasource = getValidURLs(result[0]["url"], "");
				if (datasource.indexOf("youtube.com/embed") >= 0) {
					livestream_iframe.attr("src", datasource + "?autoplay=1&&mute=1");
				}
			}

			$(".watch-here").attr("data-url", result[0]["url"]);
		}
	});

	$(".watch-here").click(function (e) {
		e.preventDefault();
		cardClickHandler($(this));
	});

	const getValidURLs = (src, src_embed = "") => {
		var embed_url = "www.youtube.com/embed/";
		if (src_embed) {
			src = src_embed;
		}
		var src_val = src.split("?");
		var src_ar = src_val[0].split("/");
		var protocol = src_ar[0] + "//";
		if (src_val[0].indexOf("youtu.be") >= 0) {
			src = protocol + embed_url + src_ar[src_ar.length - 1];
		} else if (
			src_val[0].indexOf("youtube.com") >= 0 &&
			src.indexOf("youtube.com/embed") == -1
		) {
			var src_ar_param = src_val[1].split("&");
			for (var c = 0; c < src_ar_param.length; c++) {
				var param = src_ar_param[c].split("=");
				if ((param[0] = "v")) {
					src = protocol + embed_url + param[1];
					c = src_ar_param.length;
				}
			}
		}
		return src;
	};
});
