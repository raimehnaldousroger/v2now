var getUrl = window.location;
var baseUrl =
	getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split("/")[1];

var server = {
	CISERVICE: baseUrl + "/iptv_api_v2/",
};

var viewRoutes = {
	// 'HOME'               		  : server.CISITE + '/',
	// 'LOGIN'                       : server.CISITE + '/signin',
	// 'LOGOUT'                      : server.CISITE + '/logout',
	// 'REGISTER'                    : server.CISITE + '/registration',
};

var endPoints = {
	watch: server.CISERVICE + "Watch/",
	trending: server.CISERVICE + "Trending/",
	favorite: server.CISERVICE + "Favorite/",
	explore: server.CISERVICE + "Explore/",
	read: server.CISERVICE + "Read/",
	local: server.CISERVICE + "Local/",
	member: server.CISERVICE + "member/",
};

/** LOCAL WEB STORAGE ITEMS **/
var STORAGE_ITEM = {
	TOKEN: "Token",
	LOGIN: "Login",
};

var CONFIG = {
	HEADER: {
		headers: {
			Authorization: "Basic " + btoa("webPortal" + ":" + "VV313p0Rt@l"),
			"X-API-KEY": "4cww48g0cggc4kgggsckg8wo4kk8k8wowwgooo44",
		},
	},
};
